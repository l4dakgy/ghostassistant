package main

// Functions that update and check application state

func activeOperation() int {
	var active int
	for _, s := range jobs[0].Operations {
		if s.Active == "true" {
			active = 1
		}
		if s.Active == "false" {
			active = 0
		}
	}
	return active
}

func runningOperation() int {
	var active int
	for _, s := range jobs[0].Operations {
		if s.Running == "true" {
			active = 1
		}
		if s.Running == "false" {
			active = 0
		}
	}
	return active
}

func deactivateOperation(id string) {
	for _, s := range jobs[0].Operations {
		if s.ID == id {
			s.Active = "false"
		}
	}
}

func activateOperation(id string) {
	for _, s := range jobs[0].Operations {
		if s.ID == id {
			s.Active = "true"
		}
	}
}

func runOperation(id string) {
	for _, s := range jobs[0].Operations {
		if s.ID == id {
			s.Running = "true"
		}
	}
}

func activateJob(id string) {
	for _, s := range jobs {
		if s.ID == id {
			s.Active = "true"
		}
	}
}
