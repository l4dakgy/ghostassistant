package main

import (
	"fmt"
	"html/template"
)

// Loads the given template file and registers template functions
func loadTemplate(name string) *template.Template {
	tmpl, err := template.New("").Funcs(template.FuncMap{
		"runOperationTmpl":  runOperationTmpl,
		"loadJobTmpl":       loadJobTmpl,
		"loadJobsTmpl":      loadJobsTmpl,
		"loadOperationTmpl": loadOperationTmpl,
	}).ParseFiles(name)
	if err != nil {
		fmt.Println(err)
	}
	return tmpl
}

// Template functions take an id string and return a JSON string for a button
func runOperationTmpl(id string) string {
	return `{ "id":"runOperation", "params":{ "id": "` + id + `" }}`
}

func loadJobTmpl(id string) string {
	return `{ "id":"loadJob", "params":{ "id": "` + id + `" }}`
}

func loadOperationTmpl(id string) string {
	return `{ "id":"loadOperation", "params":{ "id": "` + id + `" }}`
}

func loadJobsTmpl() string {
	return `{ "id":"loadJobs" }`
}
