package main

import (
	"fmt"
	"net/http"
	"os"

	"golang.org/x/net/websocket"
)

//Jobs - type declaration for a slice of jobs
type Jobs []Job

//Job - type declaration for a job
type Job struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	ID      string `json:"id"`
	Date    string `json:"date"`
	Active  string `json:"active"`
	Start   string `json:"start"`
	End     string `json:"end"`
	Tools   []struct {
		Name     string `json:"name"`
		Material string `json:"material"`
		Diameter string `json:"diameter"`
		Length   string `json:"length"`
	} `json:"tools"`
	Operations []struct {
		ID      string `json:"id"`
		Next    string `json:"next"`
		Running string `json:"running"`
		Active  string `json:"active"`
		Type    string `json:"type"`
		Tool    string `json:"tool"`
		Photo   string `json:"photo"`
		Gcode   string `json:"gcode"`
		Text    string `json:"text"`
	} `json:"operations"`
}

var jobs Jobs

// Call - type declaration for a JSON API call
type Call struct {
	ID     string `json:"id"`
	Params struct {
		ID string `json:"id"`
	} `json:"params"`
}

// Loads the homepage template and sends it to the browser
func homePage(w http.ResponseWriter, r *http.Request) {

	homeTemplate := loadTemplate("templates/homePage.html")

	err := homeTemplate.ExecuteTemplate(w, "homePage.html", jobs[0])
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}
}

func jsonAPI(ws *websocket.Conn) {
	for {
		var msg Call

		err := websocket.JSON.Receive(ws, &msg)
		if err != nil {
			fmt.Println(err)
			break
		}

		if runningOperation() == 1 {
			updateConsole(ws, "Operation is currently running...")
		}

		if msg.ID == "runOperation" {
			go runOperationAPI(ws, msg)
		}

		if msg.ID == "loadJob" {
			go loadJobAPI(ws, msg)
		}

		if msg.ID == "loadJobs" {
			go loadJobsAPI(ws, msg)
		}

		if msg.ID == "loadOperation" {
			go loadOperationAPI(ws, msg)
		}

		updateConsole(ws, msg.ID)

	}
}

func main() {

	var err error

	httpPort := os.Getenv("PORT")
	if httpPort == "" {
		httpPort = "3000"
	}

	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("assets"))
	if err != nil {
		panic(err)
	}

	startJob("00001")

	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))
	mux.HandleFunc("/", homePage)
	mux.Handle("/api", websocket.Handler(jsonAPI))
	fmt.Println("The HTTP server is running on port:", httpPort)
	http.ListenAndServe(":"+httpPort, mux)
}
