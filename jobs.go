package main

import (
	"encoding/json"
	"fmt"
	"os"
)

// Functions for dealing with job structs

// Loads the job file from the filesystem and returns a job struct
func loadJobFile(a string) Job {
	var job Job
	jobFile, err := os.Open("assets/routine_glock_rails.json")
	if err != nil {
		fmt.Println(err)
	}
	err = json.NewDecoder(jobFile).Decode(&job)
	if err != nil {
		fmt.Println(err)
	}
	return job
}

// Appends the jobs slice with the loaded job struct
func startJob(id string) {
	jobs = append(jobs, loadJobFile(id))
}
