package main

// Websocket API functions take a websocket connection and a call

import (
	"bytes"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/websocket"
)

func updateConsole(ws *websocket.Conn, text string) {
	var tpl bytes.Buffer

	consoleComponent := loadTemplate("templates/consoleComponent.html")

	err := consoleComponent.ExecuteTemplate(&tpl, "consoleComponent.html", text)
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}

	result := tpl.String()

	err = websocket.Message.Send(ws, result)
	if err != nil {
		fmt.Println(err)
	}
}

func runOperationAPI(ws *websocket.Conn, call Call) {
	runOperation(call.Params.ID)
	updateConsole(ws, "Operation currently running...")
}

func loadOperationAPI(ws *websocket.Conn, call Call) {
	var tpl bytes.Buffer

	activateOperation(call.Params.ID)

	operationComponent := loadTemplate("templates/operationComponent.html")

	err := operationComponent.ExecuteTemplate(&tpl, "operationComponent.html", jobs[0].Operations[0])
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}

	result := tpl.String()

	time.Sleep(3 * time.Second)
	err = websocket.Message.Send(ws, result)
	if err != nil {
		fmt.Println(err)
	}
}

func loadJobAPI(ws *websocket.Conn, call Call) {
	var tpl bytes.Buffer

	activateJob(call.Params.ID)

	jobComponent := loadTemplate("templates/jobComponent.html")

	err := jobComponent.ExecuteTemplate(&tpl, "jobComponent.html", jobs[0])
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}

	//load result into string buffer
	result := tpl.String()

	//outgoing data to update the page
	time.Sleep(3 * time.Second)
	err = websocket.Message.Send(ws, result)
	if err != nil {
		fmt.Println(err)
	}
}

func loadJobsAPI(ws *websocket.Conn, call Call) {
	var tpl bytes.Buffer

	jobsComponent := loadTemplate("templates/jobsComponent.html")

	err := jobsComponent.ExecuteTemplate(&tpl, "jobsComponent.html", jobs[0])
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}

	result := tpl.String()

	time.Sleep(3 * time.Second)
	err = websocket.Message.Send(ws, result)
	if err != nil {
		fmt.Println(err)
	}
}
